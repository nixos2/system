{pkgs, ...}: {

environment.systemPackages = with pkgs; [
  # Network utils
  wget
  curl
  
  telnet
  ncat
  mosh
  tcpdump
  wireshark
  dnsutils
  nmap
  transmission-gtk
  clipgrab

  #### Maximum
  # Tutor cloud
  skaffold
  kubernetes-helm
  kubectl


  # Dev utils
    git

  # System utils
  clutter
  clutter-gtk
  cogl
  gnome3.clutter
  gnome3.cogl
  gnome3.clutter-gtk
  gnome3.clutter-gst
  xorg.libXinerama
  coreutils
  gdb
  wineFull
  gnome3.gnome-tweaks
  gnome3.gnome-shell-extensions
  gnome3.gnome-flashback
  gnome3.vte
  htop
  xdotool
  psmisc
  usbutils
  hwinfo
  pkg-config
  gobjectIntrospection
  webkitgtk
  gtksourceview
  libgudev
  xclip
  dmenu
  jq
  qemu
  virtmanager


  #### Text editors
  # emacs
  neovim
  
  gnome3.dconf

  # Build utils
  automake
  gnumake

  # Drivers
  mesa
  mesa_drivers
  driversi686Linux.mesa

  # Libs
  libpqxx
  libv4l
  ffmpeg-full
  zlib
  pango
  glib

  # Themes
  adementary-theme

  # DE
  xmonad-with-packages
  haskellPackages.xmobar
  #haskellPackages.xmonadContrib
  #haskellPackages.xmonadExtras
 ];
}
