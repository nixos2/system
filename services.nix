{
  imports = [
    ./confFiles/asterisk/pjsip.conf.nix
    ./confFiles/asterisk/extensions.conf.nix
    ./confFiles/asterisk/http.conf.nix
    ./confFiles/asterisk/ari.conf.nix
  ];
  services = {
    pantheon.apps.enable = true;
    cron = {
      enable = true;
    };
    ntp = {
      enable = true;
    };
    clamav = {
      daemon = {
        enable = true;
      };
      updater = {
        enable = true;
      };
    };
    emacs = {
      enable = true;
      install = true;
      defaultEditor = true;
      # package = import /home/apterion/.emacs.d;
    };
    mongodb = {
      enable = true;
      bind_ip = "0";
      enableAuth = true;
      initialRootPassword = "12345678";
    };
    postgresql.enable = true;
    teamviewer.enable = true;
    xserver = {
      displayManager = {
        gdm.enable = true;
        lightdm.enable = false;
      };
      windowManager = {
        xmonad = {
	        enable = true;
	        enableContribAndExtras = true;
          extraPackages = haskellPackages: [
            haskellPackages.xmonad
            haskellPackages.xmonad-contrib
            haskellPackages.xmonad-extras
          ];
	      };
        awesome = {
          enable = true;
          noArgb = false;
        };
        bspwm = {
          enable = true;
        };
      };
      desktopManager = {
        gnome3 = {
          enable = true;
        };
        xfce = {
          enable = false;
        };
        lxqt = {
          enable = false;
        };
        plasma5 = {
          enable = false;
        };
        pantheon = {
          enable = false;
        };
        mate = {
          enable = false;
        };
        lumina = {
          enable = false;
        };
        kodi = {
          enable = false;
        };
      };
      enable = true;
      layout = "ru";
      libinput = {
        enable = true;
      };
    };
    printing = {
      enable = true;
    };
    openssh = {
      enable = true;
    };
    openvpn = {
      servers = {
        officeVPN  = { config = '' config /home/apterion/openvpn/maximum-test01_sergey.shavlovskiy@maximumtest.ru_admin-test01.ovpn ''; };
      };
    };
  };
}
