{
  "sip.conf" = ''
    [101]
    type = friend
    host = dynamic
    username = 101
    secret = 101
    context = from-10x
    callerid = 4953639285
    qualify = yes

    [102]
    type = friend
    host = dynamic
    username = 102
    secret = 102
    context = from-10x
    callerid = 4953639285
    qualify = yes
    '';
}