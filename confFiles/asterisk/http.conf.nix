{
  services.asterisk.confFiles = {
      "http.conf" = ''
        [general]
        enabled=yes
        bindaddr=127.0.0.1
        bindport=8088
        servername=Asterisk
      '';
  };
}