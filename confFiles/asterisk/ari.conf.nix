{
  services.asterisk.enable = true;
  services.asterisk.useTheseDefaultConfFiles = [
      "acl.conf"
      "agents.conf"
      "amd.conf"
      "calendar.conf"
      "cdr.conf"
      "cdr_syslog.conf"
      "cdr_custom.conf"
      "cel.conf"
      "cel_custom.conf"
      "cli_aliases.conf"
      "confbridge.conf"
      "dundi.conf"
      "features.conf"
      "hep.conf"
      "iax.conf"
      "pjsip_wizard.conf"
      "phone.conf"
      "phoneprov.conf"
      "queues.conf"
      "res_config_sqlite3.conf"
      "res_parking.conf"
      "statsd.conf"
      "udptl.conf"
      "unistim.conf"
  ];
  services.asterisk.confFiles = {
    "ari.conf" = ''
      [general]
      enabled = yes
      pretty = yes
      allowed_origins = *

      [arihs]
      type = user
      read_only = no
      password = 123
      password_format = plain
    '';
  };
}