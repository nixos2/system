{
  services.asterisk.confFiles = {
    "pjsip.conf" = ''
;--
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Non mapped elements start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[101]
dtmf_mode = auto ; did not fully map - set to none

[102]
dtmf_mode = auto ; did not fully map - set to none

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Non mapped elements end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
--;


[transport-udp]
type = transport
protocol = udp
bind = 0.0.0.0

[101]
type = aor
max_contacts = 10

[101]
type = auth
username = 101
password = 101

[101]
type = endpoint
context = from-10x
dtmf_mode = none
disallow = all
allow = gsm,ilbc,g722,alaw,ulaw
rtp_symmetric = yes
force_rport = yes
rewrite_contact = yes
direct_media = no
callerid = 4952233181
auth = 101
outbound_auth = 101
aors = 101
accountcode = 101

[102]
type = aor
max_contacts = 10

[102]
type = auth
username = 102
password = 102

[102]
type = endpoint
context = from-10x
dtmf_mode = none
disallow = all
allow = gsm,ilbc,g722,alaw,ulaw
rtp_symmetric = yes
force_rport = yes
rewrite_contact = yes
direct_media = no
callerid = 4952233182
auth = 102
outbound_auth = 102
aors = 102
accountcode = 101
    '';
  };
}
