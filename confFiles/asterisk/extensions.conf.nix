{
  services.asterisk.confFiles = {
    "extensions.conf" = ''
      [from-10x]
      ; Dial 100 for "hello, world"
      exten => 200,1,Answer()
      same  =>     n,Wait(1)
      same  =>     n,Playback(hello-world)
      same  =>     n,Hangup()

      exten => 101,1,NoOp()
      same =>       n,Stasis(arihs)
      same  =>      n,Hangup()

      exten => 102,1,NoOp()
      same =>       n,Stasis(arihs)
      same  =>      n,Hangup()
    '';
  };
}